# Weekly Influenza Reports by Country

## Source
https://www.kaggle.com/lachmann12/weekly-influenza-reports-by-country

## Context
Ein datenbasiertes Frühwarnsystem zur Erkennung, Reaktion und Kontrolle von Epiedemien und Pandemien in Deutschland und Europa, basiert auf internationalen Big Data kommend aus vertraulichen medizinischen Quellen wie die Worl Health Organization Data Set, Robert Koch Institut Open Data, aber auch aus Social Media Quellen, News und Blogs. Eindeutiger Warnungshinweis zur Untersuchungen und Reaktionen bei der Erkennung eines ungewöhnlichen Muster oder Ereignis im Datenmodell.

## Content
This dataset is a collection of weekly reports that are submitted to the WHO FluNet by 167 countries. The number of reports is only a small subset of the actual infections and are not directly comparable across years or between countries.


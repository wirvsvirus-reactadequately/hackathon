# Google Trend Data

## Source
[Google Trends](https://trends.google.de/)

## Context
We use google search trends for specific words to identify trends and anomalies.

## Content
We limit the POC scope to Germany and specific flue related terms. Google Trends provides normalized data about the interest in these search terms.
Based on [RKI guidelines for diffential diagnosis](https://edoc.rki.de/bitstream/handle/176904/900/20hpVDYNzkNaI.pdf?sequence=1&isAllowed=y) we selected following search terms:

symptom | search terms
------- | ------------
Fieber  | 'Fieber'
Abgeschlagenheit | 'Abgeschlagenheit', 'Müdigkeit', 'müde'
Muskel-/Gliederschmerzen | 'Muskelschmerzen', 'Gliederschmerzen'
Kopfschmerzen | 'Kopfschmerzen'
Respiratorische Beschwerden | 'Respiratorische Beschwerden' (not enough data available), Husten', 'Atemgeräusche', 'Atemnot', 'Atemschmerzen'
Pharyngitis | 'Pharyngitis', 'Halsschmerzen', 'Schluckbeschwerden', 
Rhinitis | 'Rhinitis', Schnupfen' 
Konjunktivitis | 'Konjunktivitis', 'Bindehautentzündung', 'gerötete Augen'
(Hämorrhagisches) Enanthem | '(Hämorrhagisches) Enanthem' (not enough data available), Ausschlag'
Lymphozytopenie  | 'Lymphozytopenie' (not enough data available)
LDH-Erhöhung | 'LDH-Erhöhung' (not enough data available)
Diarrhö | 'Diarrhö', 'Durchfall'





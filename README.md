# Pandiman

## What it does
Pandiman ist ein datenbasiertes Frühwarnsystem zur Erkennung und Prognose von Epiedemien und Pandemien in Deutschland und Europa.
Es basiert in der POC-Variante auf den Google Trends Daten zu Suchanfragen. Wir haben solche Suchanfragen nach Symptomen ausgewählt, die bei Differential-Diagnosen von Infektionskrankheiten von Ärtzen abgefragt werden.

## Vorgehen
Zunächst haben wir uns die WHO-Daten zu früheren Edpidemien von Influenza und Schweinegrippe besorgt. Anhand der Anleitung zur Differential-Diagnose des RKI haben wir die Liste der Suchbegriffe ermittelt. Bei Google Trends haben wir die Daten zum Interesse an den Suchbegriffen gecrawled.
Die Daten haben wir analysiert und ein Modell aufgestellt, dass das Auftreten einer Epidemie der bekannten Influenza-Viren vorhersagen soll.

## Ergebnis
Das Modell kann mit einer etwa 85% Genauigkeit das Auftreten der verschiedenen Influenza-Typen und Sars nur anhand der Suchtrends vorhersagen.

## Next Steps
Um die Genauigkeit des Modells zu verbessern, sind weitere Datenquallen wie z.B. Krankmeldungs-Statistiken anzubinden und die Menge der Suchbegriffe zu verfeinern.
